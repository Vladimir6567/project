def check(a, b, c):
    """
    Функция проверки введённых данных.
    Проверка трех чисел на положительность.

    Args:
        a: Первое число.
        b: Второе число.
        c: Третье число.

    Returns:
        Возвращает True если все три числа положительные, иначе возвращает False.

    Raises:
        TypeError.

    Examples:
        >>> check(1,1,1)
        True

        >>> check(-1,1,1)
        False

        >>> check("a","a","a")
        Traceback (most recent call last):
            ...
        TypeError: '>' not supported between instances of 'str' and 'int'

    """
    return a > 0 and b > 0 and c > 0


def alg(a, b, n):
    """
    Функция, вычисляющая члены последовательности.

    Принимает на вход три положительных числа. По первым двум вычисляет третий член последовательности логическим xor.

    Args:
        a: Первый член последовательности.
        b: Второй член последовательности.
        n: Длина последовательности.

    Return:
        Возвращает список членов последовательности и длину последовательности.

    Examples:
        >>> alg(3,4,1)
        ((3, 4, 7), 1)

        >>> alg(-3,1,1)
        Не коректно введённые данные!

    """
    if check(a, b, n):
        c = a ^ b
        ans = (a, b, c)
        return ans, n
    else:
        print("Не коректно введённые данные!")
        return None


def test_check():
    """
    Тест функии check.
    Первый случай: все числа положительные.
    Второй - пятый случаи: есть отрицательные числа.
    """
    assert check(10, 10, 10) is True
    assert check(-10, 10, 10) is False
    assert check(10, -10, 10) is False
    assert check(10, 10, -10) is False
    assert check(-10, -10, -10) is False


def test_alg():
    """
    Тест функции alg
    Первый случай: корректные данные.
    Второй случай: некоректные данные.
    """
    assert alg(3, 4, 2) == ((3, 4, 7), 2)
    assert alg(3, -4, 2) is None
